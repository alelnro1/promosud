$(document).ready(function() {

    var MaxInputs       = 20; //Número Maximo de Campos
    var contenedor       = $("#div_a_repetir"); //ID del contenedor
    var AddButton       = $("#agregarCampo"); //ID del Botón Agregar

    //var x = número de campos existentes en el contenedor
    var x = $("#contenedor div").length + 1;
    var FieldCount = x-1; //para el seguimiento de los campos

    $(AddButton).click(function (e) {
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            //agregar campo
            $(contenedor).append(

                '<div class="row" id="campo_'+ FieldCount +'">'
                    +'<div class="col-md-4 rango" >'
                        +'<input type="number" class="form-control" name="desde[]"    placeholder="Desde">'
                    +'</div>'
                    +'<div class="col-md-4 rango">'
                        +'<input type="number" class="form-control" name="hasta[]"    placeholder="Hasta">'
                    +'</div>'
                    +'<div class="col-md-4 rango">'
                        +'<input step="0.01" type="number" class="form-control" name="importe[]"  placeholder="Importe">'
                    +'</div>'
                    +'<a href="#" class="eliminar">&times;</a>'
                +'</div>'
            );
            x++; //text box increment
        }
        return false;
    });

    $("body").on("click",".eliminar", function(e){ //click en eliminar campo
        if( x > 1 ) {
            $(this).parent('div').remove(); //eliminar el campo
            x--;
        }
        return false;
    });

    $("#file12").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-md",
        fileType: "many",
        minFileCount:1,
        maxFileCount:20,
        uploadAsync:true,
    });
    $("#formulario").submit(function (e) {
        $(".rango input").each(function (index, element) {
            if ($(this).val() == "") {
                alert('No puede haber Rangos ni importes vacios');
                e.preventDefault();
                return false;
            }
        });
    })
});