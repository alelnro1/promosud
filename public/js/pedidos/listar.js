$(document).ready(function() {
    oTable = $('#pedidos').DataTable({
        "order": [[ 0, "desc" ]],
        responsive: true,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "language": {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ pedidos filtrados",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "lengthMenu": "Mostrar _MENU_ pedidos",
            "search": "Buscar:",
            "infoFiltered": "(de un total de _MAX_ pedidos)",
        }
    });
});


