<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion');
            $table->string('archivo');
            $table->string('estado');
            $table->string('costo');
            $table->integer('categoria_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('productos', function(Blueprint $table) {
            $table->foreign('categoria_id')->references('id')->on('categorias')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function(Blueprint $table) {
            $table->dropForeign('productos_FOREIGN_ID_foreign');
        });
        
        Schema::drop('productos');
    }
}
