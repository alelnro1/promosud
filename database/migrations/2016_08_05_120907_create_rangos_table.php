<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRangosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rangos', function(Blueprint $table) {
            $table->increments('id');
            $table->float('importe');
            $table->integer('desde');
            $table->integer('hasta');
            $table->integer('producto_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('rangos', function($table) {
            $table->foreign('producto_id')->references('id')->on('productos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rangos', function(Blueprint $table) {
            $table->dropForeign('rangos_FOREIGN_ID_foreign');
        });

        Schema::drop('rangos');
    }
}






