<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('cantidad');
            $table->string('color');
            $table->string('archivo');
            $table->text('descripcion');
            $table->text('blog_cambio_estado');
            $table->string('descuento');
            $table->float('costo_unitario');
            $table->float('importe_unitario');
            $table->integer('producto_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->integer('pedido_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table('items', function(Blueprint $table) {
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('pedido_id')->references('id')->on('pedidos');
            $table->foreign('estado_id')->references('id')->on('estados_items')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function(Blueprint $table) {
            $table->dropForeign('items_FOREIGN_ID_foreign');
        });
        
        Schema::drop('items');
    }
}
