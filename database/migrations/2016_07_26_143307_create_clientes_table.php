<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion');
            $table->text('texto');
            $table->string('archivo');
            $table->string('domicilio_fiscal');
            $table->string('domicilio_comercial');
            $table->string('email');
            $table->string('telefono_fijo');
            $table->string('telefono_celular');
            $table->string('persona_contacto');
            $table->string('pagina_web');
            $table->string('condicion_iva'); // Condicion frente al iva
            $table->string('cuit');
            $table->string('cuil');
            $table->timestamps();
            $table->softDeletes();
        });
    
        /*Schema::table('clientes', function($table) {
            //$table->foreign('RELACION_ID')->references('id')->on('OTRA_TABLA');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('clientes', function(Blueprint $table) {
            $table->dropForeign('clientes_FOREIGN_ID_foreign');
        });*/
        
        Schema::drop('clientes');
    }
}
