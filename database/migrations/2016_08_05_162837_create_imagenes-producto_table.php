<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenesProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes_productos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('archivo');
            $table->integer('producto_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('imagenes_productos', function(Blueprint $table) {
            $table->foreign('producto_id')->references('id')->on('productos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imagenes_productos', function(Blueprint $table) {
            $table->dropForeign('imagenes_productos_FOREIGN_ID_foreign');
        });

        Schema::drop('imagenes_productos');
    }
}
