<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function(Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->string('archivo');
            $table->string('estado');
            $table->timestamps();
            $table->softDeletes();
    
        });
    
        Schema::table('categorias', function($table) {
            //$table->foreign('RELACION_ID')->references('id')->on('OTRA_TABLA');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function(Blueprint $table) {
            $table->dropForeign('categorias_FOREIGN_ID_foreign');
        });
        
        Schema::drop('categorias');
    }
}
