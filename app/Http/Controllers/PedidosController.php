<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Cliente;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use App\Pedido;
use Symfony\Component\HttpKernel\Client;

class PedidosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedido::with('Items')->get();
        return view('pedidos.listar')->with('pedidos', $pedidos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::orderby('nombre','asc')->get();
        return view('pedidos.create')->with('clientes', $clientes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|max:500',
            'cliente_id'     => 'required',
            'fecha'       => 'required|date',
        ]);

        if ($validator->fails())
            return redirect('pedidos/create')->withErrors($validator)->withInput();

        // Creo el pedido
        $pedido = Pedido::create($request->all());

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $pedido);

        if ($validator) {
            if ($validator->fails())
                return redirect('pedidos/create')->withErrors($validator)->withInput();
        }

        return redirect('/pedidos/')->with('pedido_creado', 'Pedido con nombre ' . $request->nombre . ' creado');
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $pedido
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $pedido)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $pedido->archivo = $archivo['url'];
                $pedido->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedido::findOrFail($id);
        return view('pedidos.show')->with('pedido', $pedido);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = Pedido::findOrFail($id);
        $clientes = Cliente::all();
        return view('pedidos.edit', array('clientes' => $clientes, 'pedido' => $pedido));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required|max:500',
            'cliente_id'     => 'required',
            'fecha'       => 'required|date',
        ]);

        if ($validator->fails())
            return redirect('pedidos/' . $id .'/edit')->withErrors($validator)->withInput();

        // Busco el pedido
        $pedido = Pedido::findOrFail($id);

        // Actualizo el pedido
        $pedido->update($request->except(['_method', '_token']));

       // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $pedido);

        if ($validator) {
            if ($validator->fails())
                return redirect('pedidos/create')->withErrors($validator)->withInput();
        }

        return redirect('/pedidos')->with('pedido_actualizado', 'Pedido actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = Pedido::findOrFail($id);

        $pedido->delete();

        return redirect('/pedidos/')->with('pedido_eliminado', 'Pedido con nombre ' . $pedido->nombre . ' eliminado');
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }


}
