<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Item;
use App\Producto;
use App\EstadoItem;
class ItemsController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pedido_id)
    {
        $items = Item::where('pedido_id',$pedido_id)->get();
        return view('items.listar', array( 'items' => $items, 'pedido_id' => $pedido_id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pedido_id)
    {
        $productos = Producto::all();
        $estados = EstadoItem::all();

        return view('items.create', array( 'productos' => $productos, 'estados' => $estados, 'pedido_id' => $pedido_id));
    }


    public function getEstadoSegunDescripcion($descripcion){
        $estado =  EstadoItem::where('descripcion', $descripcion)->first();
        return $estado->id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['estado_id'] = $this->getEstadoSegunDescripcion('Recibido');
        $producto = Producto::find($request->producto_id);
        $request['importe_unitario'] = $producto->calcularImporteUnitarioSegunRango($request->cantidad);
        $request['costo_unitario'] = $producto->costo;
        // Valido el input
        $validator = Validator::make($request->all(), [
            'producto_id'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'archivo'     => 'required|max:1000|mimes:jpg,jpeg,png,gif',
            'cantidad'    => 'required|max:1000',
            'estado_id'   => 'required',
            'pedido_id'   => 'required',
            'importe_unitario' =>'required',
        ]);

        if ($validator->fails()) {
            if (!$request['importe_unitario']) {
                $validator->errors()->add('cantidad_rango', 'La cantidad seleccionada no pertenece a un rango cargado para el producto seleccionado');
            }
            return redirect('items/create/' . $request->pedido_id)->withErrors($validator)->withInput();
        }
        // Creo el item
        $item = Item::create($request->all());

        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $item);

        if ($validator) {
            if ($validator->fails())
                return redirect('items/create/'. $request->pedido_id)->withErrors($validator)->withInput();
        }

        return redirect('/items/index/'. $request->pedido_id)->with('item_creado', 'Item con nombre ' . $request->nombre . ' creado');
    }

    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $item
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $item)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($request);

            if ($archivo['url']) {
                $item->archivo = $archivo['url'];
                $item->save();
            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::findOrFail($id);
        return view('items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);
        $productos = Producto::all();
        $estados = EstadoItem::all();
        return view('items.edit', array( 'productos' => $productos, 'estados' => $estados, 'item' => $item));
    }
    public function editEstado($id)
    {
        $item = Item::findOrFail($id);
        $estados = EstadoItem::all();
        return view('items.estado', array( 'estados' => $estados, 'item' => $item));
    }

    public function updateEstado(Request $request, $id)
    {
        $item = Item::find($id);
        $item->estado_id = $request->estado_id;
        $estado = EstadoItem::find($request->estado_id);
        $texto ="<p>Nuevo Estado: <b>". $estado->descripcion . "</b><br> Usuario: " . Auth::user()->name ."<br> Fecha:". date('d/m/Y') . "   Hora:". localtime()[2] . ":" . localtime()[1]. ":" . localtime()[0] . "</p>".
            "<p style='color:blue;'><b>Comentario: </b>\". $request->blog_cambio_estado .\"</p> ".
            "<hr>";
        $item->blog_cambio_estado = $texto .$item->blog_cambio_estado ;
        $item->save();
        return redirect('/items/index/'. $item->pedido_id);


    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $producto = Producto::find($request->producto_id);
        $request['importe_unitario'] = $producto->calcularImporteUnitarioSegunRango($request->cantidad);
        $request['costo_unitario'] = $producto->costo;
        // Valido el input
        $validator = Validator::make($request->all(), [
            'producto_id'   => 'required|max:100',
            'descripcion'   => 'required|max:500',
            'cantidad'      => 'required|max:1000',
            'importe_unitario' =>'required',
        ]);

        if ($validator->fails()) {
            if (!$request['importe_unitario']) {
                $validator->errors()->add('cantidad_rango', 'La cantidad seleccionada no pertenece a un rango cargado para el producto seleccionado');
            }
            return redirect('items/edit/' . $id)->withErrors($validator)->withInput();
        }

        // Busco el item
        $item = Item::findOrFail($id);
        // Actualizo el item
        $item->update($request->except(['_method', '_token']));
        // Si se trató de guardar una foto para el local, validarla y subirla
        $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $item);

        if ($validator) {
            if ($validator->fails())
                return redirect('items/edit/'. $id)->withErrors($validator)->withInput();
        }
        return redirect('/items/index/'. $item->pedido_id)->with('item_actualizado', 'Item actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $pedido_id =$item->pedido_id;
        $item->delete();

        return redirect('/items/index/'.$pedido_id)->with('item_eliminado', 'Item con nombre ' . $item->nombre . ' eliminado');
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo(Request $request)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $request->archivo->getClientOriginalName();
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($request->archivo->isValid()) {
            if ($request->archivo->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $request->archivo->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }


}
