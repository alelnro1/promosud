<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use App\Producto;
use App\Rango;
use App\ImagenProducto;

class ProductosController extends Controller
{
    use SoftDeletes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::with('Categoria')->get();
        return view('productos.listar')->with('productos', $productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias =Categoria::all();
        return view('productos.create')->with('categorias' , $categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'archivo'     => '',
            'costo'       => 'required',
            'categoria_id'    => 'required|exists:categorias,id',
        ]);
        
        if ($validator->fails())
            return redirect('productos/create')->withErrors($validator)->withInput();
        
        // Creo el producto
        $producto = Producto::create($request->all());

        $this->crearRangos($request, $producto->id);

        $this->validarCantidadDeArchivosYmandarAguardar($request, $validator, $producto,'create');

        return redirect('/productos/')->with('producto_creado', 'Producto con nombre ' . $request->nombre . ' creado');
    }
    
    /**
     * Si hay algun archivo para subir, subirlo y guardar la referencia en la base
     * @param $request
     * @param $validator
     * @param $producto
     * @return mixed
     */
    private function subirYGuardarArchivoSiHay($request, $validator, $producto,$archivo_a_subir)
    {
        if (isset($request->archivo) && count($request->archivo) > 0) {
            $archivo = $this->subirArchivo($archivo_a_subir);

            if ($archivo['url']) {

                ImagenProducto::create([
                    'producto_id' => $producto->id,
                    'archivo' => $archivo['url']
                ]);

            } else {
                $validator->errors()->add('archivo', $archivo['err']);

                return $validator;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::findOrFail($id);
        return view('productos.show')->with('producto', $producto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::findOrFail($id);
        $categorias =Categoria::all();
        return view('productos.edit', array('categorias' => $categorias, 'producto' => $producto));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Valido el input
        $validator = Validator::make($request->all(), [
            'nombre'      => 'required|max:100',
            'descripcion' => 'required|max:500',
            'archivo'     => '',
            'costo'       => 'required',
            'costo'       => 'required',
            'categoria_id'    => 'required|exists:categorias,id',
        ]);

        if ($validator->fails()) 
            return redirect('productos/' . $id .'/edit')->withErrors($validator)->withInput();

        // Busco el producto
        $producto = Producto::findOrFail($id);
        Rango::where('producto_id', $producto->id)->delete();
        $this->crearRangos($request,$producto->id);
        $this->validarCantidadDeArchivosYmandarAguardar($request, $validator, $producto, 'update');

        // Actualizo el producto
        $producto->update($request->except(['_method', '_token']));


        return redirect('/productos')->with('producto_actualizado', 'Producto actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::findOrFail($id);

        ImagenProducto::where('producto_id', $producto->id)->delete();
        Rango::where('producto_id', $producto->id)->delete();
        $producto->delete();

        return redirect('/productos/')->with('producto_eliminado', 'Producto con nombre ' . $producto->nombre . ' eliminado');
    }

        /**
     * Subir un archivo
     * @param Request $request
     * @return JSON
     */
    public function subirArchivo($archivo_a_subir)
    {
        $directorio_destino = 'uploads/archivos/';
        $nombre_original    = $archivo_a_subir->getClientOriginalName();
        $extension          = $archivo_a_subir->getClientOriginalExtension();
        $nombre_archivo     = rand(111111,999999) .'_'. time() . "_.". $extension;

        if ($archivo_a_subir->isValid()) {
            if ($archivo_a_subir->move($directorio_destino, $nombre_archivo)) {
                $url = $directorio_destino . $nombre_archivo;
                $error = false;
            } else {
                $url = false;
                $error = "No se pudo mover el archivo";
            }
        } else {
            $url = false;
            $error = $archivo_a_subir->getErrorMessage();
        }

        return array('url' => $url, 'err' => $error);
    }

    public function eliminarImagen($id_imagen_producto)
    {
        ImagenProducto::where('id',$id_imagen_producto)->delete();
        echo json_encode(['valid' => true]);
    }

    private function crearRangos($request, $id)
    {
        foreach($request->desde as $index=>$desde )
        {
            $rango = Rango::create([
                'producto_id' => $id,
                'desde' => $desde,
                'hasta' => $request->hasta[$index],
                'importe'=> $request->importe[$index],
            ]);
        }
    }

    private function validarCantidadDeArchivosYmandarAguardar($request, $validator, $producto, $funcion)
    {
        if($request->archivo[0] != null) {
            if (count($request->archivo) > 1) {
                foreach ($request->archivo as $archivo_a_subir) {

                    // Si se trató de guardar una foto para el local, validarla y subirla
                    $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $producto, $archivo_a_subir);

                    if ($validator) {
                        if ($validator->fails())
                            return redirect('productos/create')->withErrors($validator)->withInput();
                    }
                }
            } else {
                // Si se trató de guardar una foto para el local, validarla y subirla
                $validator = $this->subirYGuardarArchivoSiHay($request, $validator, $producto, $request->archivo[0]);

                if ($validator) {
                    if ($validator->fails())
                        return redirect('productos/'. $funcion)->withErrors($validator)->withInput();
                }
            }
        }
    }


}
