<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes...
$this->get('login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', 'Auth\AuthController@logout');

Route::group(['middleware'=>'auth'], function() {
    Route::get('/home', 'HomeController@index');
    Route::get('/eliminar-imagen-producto/{id_imagen} ', 'ProductosController@eliminarImagen');

//Route::group(['middleware' => 'web'], function() {
    Route::resource('clientes', 'ClientesController');
//});//Route::group(['middleware' => 'web'], function() {
    Route::resource('categorias', 'CategoriasController');
//});//Route::group(['middleware' => 'web'], function() {
    Route::resource('productos', 'ProductosController');
//});//Route::group(['middleware' => 'web'], function() {
    Route::resource('pedidos', 'PedidosController');
//});//Route::group(['middleware' => 'web'], function() {
    Route::get('items/index/{pedido_id}', 'ItemsController@index');
    Route::get('items/create/{pedido_id}', 'ItemsController@create');
    Route::get('items/show/{pedido_id}', 'ItemsController@show');
    Route::post('items', 'ItemsController@store');
    Route::get('items/edit/{item_id}', 'ItemsController@edit');
    Route::get('items/edit-estado/{item_id}', 'ItemsController@editEstado');
    Route::patch('items/{item_id}', 'ItemsController@update');
    Route::delete('items/{item_id}', 'ItemsController@destroy');
    Route::post('items/update-estado/{item_id}', 'ItemsController@updateEstado');


//});//Route::group(['middleware' => 'web'], function() {
    Route::resource('usuarios', 'UsuariosController');
//});
});