<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoItem extends Model
{
    protected $table = 'estados_items';

    public function Items()
    {
        return $this->hasMany(Item::class);
    }
}
