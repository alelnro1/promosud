<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'pedidos';

    protected $fillable = ['cliente_id','descripcion','fecha'];

    public function Cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
    public function Items(){
        return $this->hasMany(Item::class);
    }

}
