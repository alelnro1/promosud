<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenProducto extends Model
{
    protected $table = 'imagenes_productos';
    protected $fillable = [
        'producto_id', 'archivo'
    ];

    public function Producto(){
        return $this->belongsTo(Producto::class);
    }
}
