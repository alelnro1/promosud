<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';

    protected $fillable = [
        'nombre', 'descripcion', 'estado', 'costo', 'categoria_id',
    ];

    public function Rangos()
    {
        return $this->hasMany(Rango::class);
    }

    public function Categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function ImagenesProductos()
    {
        return $this->hasMany(ImagenProducto::class);
    }

    public function calcularImporteUnitarioSegunRango($cantidad)
    {
        $rangos = $this->Rangos()->get();
        foreach ($rangos as $rango) {
            if ($rango->desde <= $cantidad & $cantidad <= $rango->hasta) {
                return $rango->importe;
            }
        }
        return "";
    }
}
