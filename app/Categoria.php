<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

    protected $fillable = [
        'descripcion', 'archivo', 'estado',
    ];
    public function Productos(){
        return $this->hasMany(Producto::class);
    }
}
