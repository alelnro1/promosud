<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rango extends Model
{
    protected $table = 'rangos';
    protected $fillable = [
        'producto_id', 'importe','desde','hasta',
    ];

    public function Producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
