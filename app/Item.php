<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';

    protected $fillable = [
        'producto_id','pedido_id',  'cantidad', 'estado_id', 'color', 'archivo', 'descripcion', 'descuento', 'importe_unitario', 'costo_unitario'];

    public function Producto(){
        return $this->belongsTo(Producto::class);
    }
    public function Estado(){
        return $this->belongsTo(EstadoItem::class);
    }

}
