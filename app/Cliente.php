<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $fillable = [
        'nombre', 'descripcion', 'texto', 'archivo', 'domicilio_fiscal', 'domicilio_comercial',
        'telefono_fijo', 'telefono_celular', 'persona_contacto', 'pagina_web', 'condicion_iva', 'email', 'cuit', 'cuil'
    ];

    public function Pedidos()
    {
        return $this->hasMany(Pedido::class);
    }
}
