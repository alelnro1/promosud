@extends('layouts.app')

@section('site-name', 'Listando items')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Items  => Pedido N° {{$pedido_id}}

        <div style="float:right;">
            <a href="/items/create/{{$pedido_id}}" class="btn btn-block btn-default btn-sm">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('item_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('item_eliminado') }}
            </div>
        @endif

        @if(Session::has('item_creado'))
            <div class="alert alert-success">
                {{ Session::get('item_creado') }}
            </div>
        @endif
        
        @if(Session::has('item_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('item_actualizado') }}
            </div>
        @endif

        @if (count($items) > 0)
            <table class="table table-bordered table-hover" id="items" style="width:100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->producto->nombre }}</td>
                        <td>
                            @if(strlen( $item->descripcion) > 20){{substr($item->descripcion, 0 , 20  ) . '...'}}
                            @else {{$item->descripcion}}
                            @endif
                        </td>
                        <td>{{ $item->estado->descripcion }}</td>
                        <td class="text-right">{{ $item->cantidad }}</td>
                        <td class="text-right">{{ '$ ' .number_format(($item->importe_unitario * $item->cantidad)* (1 - $item->descuento/100),2) }}</td>

                        <td>
                            <a href="/items/show/{{ $item['id'] }}" class="btn btn-default btn-sm">Ver</a>
                            
                            <a href="/items/edit/{{ $item['id'] }}" class="btn btn-default btn-sm">Editar</a>
                            <a href="/items/edit-estado/{{ $item['id'] }}" class="btn btn-default btn-sm">Estado</a>

                            <a href="/items/{{ $item['id'] }}" class="btn btn-default btn-sm"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Está seguro que desea eliminar a item con nombre {{ $item->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay items
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/items/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/delete-link.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
