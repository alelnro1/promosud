@extends('layouts.app')

@section('site-name', 'Estado item')

@section('content')
    <div class="panel-heading">Item</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{url('items/update-estado/' . $item->id)}}"
              enctype="multipart/form-data">
            {!! csrf_field() !!}

                    <!-- estado -->
            <div class="form-group{{ $errors->has('estado_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Estado</label>

                <div class="col-md-6">
                    <select class="form-control" name="estado_id" >
                        @foreach($estados as $estado)
                            <option value="{{$estado->id}}" @if($estado->id == $item->estado_id) selected @endif>{{$estado->descripcion}}</option>
                        @endforeach
                    </select>
                    <hr>
                    <p style="color:gray;font-weight: bold;">
                        <?php echo($item->blog_cambio_estado) ?>
                    </p>
                    <hr>
                    <textarea name="blog_cambio_estado" rows="3" class="form-control col-md-6"
                              placeholder="Escriba su comentario acerca del cambio de estado"></textarea>
                    @if ($errors->has('estado_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estado_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
    </div>

@stop

@section('javascript')
@stop

