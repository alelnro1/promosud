@extends('layouts.app')

@section('site-name', 'Viendo a item')

@section('content')
    <div class="panel-heading">
        Item con nombre <b><i>{{ $item->nombre }}</i></b>
    </div>

    <div class="panel-body">

        @if(isset($item->archivo) && $item->archivo != "")
            <div class="text-center margin-bottom">
                <img src="/{{ $item->archivo }}" style="max-height: 150px;" />
            </div>
        @endif

        <table class="table table-striped task-table " style="margin-bottom: 20px;">
            <tr>
                <td><strong>Producto</strong></td>
                <td>{{ $item->producto->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $item->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $item->estado->descripcion }}
                    <!-- Small modal -->
                    <button type="button" class="btn-sm btn-info" data-toggle="modal" style="float:right" data-target=".bs-example-modal-sm">Blog Estados</button>
                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                        <div  class="modal-dialog modal-sm " role="document" style="max-height: 250px;overflow-y: scroll;">
                            <div class="modal-content">
                                <?php echo($item->blog_cambio_estado) ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td><strong>Color</strong></td>
                <td>{{$item->color}}</td>
            </tr>
            <tr>
                <td><strong>Cantidad</strong></td>
                <td>{{$item->cantidad}}</td>
            </tr>

            <tr>
                <td><strong>Descuento</strong></td>
                <td>{{ number_format($item->descuento,2) . ' %' }}</td>
            </tr>

            <tr>
                <td><strong>Costo Unitario</strong></td>
                <td>{{ '$ ' .number_format($item->costo_unitario,2) }}</td>
            </tr>

            <tr>
                <td><strong>Importe Unitario</strong></td>
                <td>{{ '$ ' .number_format($item->importe_unitario,2) }}</td>
            </tr>

            <tr>
                <td><strong>Importe Total</strong></td>
                <td>{{ '$ ' .number_format(($item->importe_unitario * $item->cantidad)* (1 - $item->descuento/100),2) }}</td>
            </tr>

            <tr>
                <td><strong>Fecha Creación</strong></td>
                <td>{{ $item->created_at }}</td>
            </tr>
        </table>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        <div class="col-xs-6">
            <a href="/items/edit/{{ $item->id }}" class="btn btn-default btn-primary" style="float:right; color: white;">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
            </a>
        </div>
    </div>
@stop
