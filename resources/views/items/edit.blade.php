@extends('layouts.app')

@section('site-name', 'Editar item')

@section('content')
    <div class="panel-heading">Item</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ url('items/' . $item->id) }}"
              enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}


                    <!-- Producto -->
            <div class="form-group{{ $errors->has('producto_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Producto</label>

                <div class="col-md-6">
                    <select type="text" class="form-control" name="producto_id">
                        @foreach($productos as $producto)
                            <option @if($producto->id == $item->producto_id) selected
                                    @endif value="{{$producto->id}}">{{$producto->nombre}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('producto_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('producto_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6"
                              placeholder="Escriba la descripción">{{ $item->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Color -->
            <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Color</label>

                <div class="col-md-6">
                    <input name="color" class="form-control col-md-6" placeholder="Escriba el Color"
                           value="{{$item->color }}">
                    @if ($errors->has('color'))
                        <span class="help-block">
                            <strong>{{ $errors->first('color') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <!-- Cantidad -->
            <div class="form-group{{ $errors->has('cantidad') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cantidad</label>

                <div class="col-md-6">
                    <input type="number" class="form-control" name="cantidad" value="{{ $item->cantidad }}"
                           placeholder="Escriba la cantidad">

                    @if ($errors->has('cantidad'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cantidad') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('cantidad_rango'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cantidad_rango') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descuento -->
            <div class="form-group{{ $errors->has('cantidad') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Descuento (%)</label>

                <div class="col-md-6">
                    <input max="100" min="0" type="number" step="0.01" class="form-control" name="descuento"
                           value="{{ $item->descuento }}" placeholder="Escriba el porcentaje descuento">

                    @if ($errors->has('descuento'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descuento') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Logo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Logo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript"
            src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAjTpj9h5ANX5iTQIKxkAhI-zcoPxl8GtY"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/items/edit.js') }}"></script>
@stop
