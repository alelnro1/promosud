@extends('layouts.app')

@section('site-name', 'Listando pedidos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Pedidos

        <div style="float:right;">
            <a href="/pedidos/create" class="btn btn-block btn-default btn-sm">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('pedido_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_eliminado') }}
            </div>
        @endif

        @if(Session::has('pedido_creado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_creado') }}
            </div>
        @endif

        @if(Session::has('pedido_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('pedido_actualizado') }}
            </div>
        @endif

        @if (count($pedidos) > 0)
            <table class="table table-bordered table-hover" id="pedidos" style="width:100%">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Código</th>
                    <th>Cliente</th>
                    <th>Descripción</th>
                    <th>Total</th>
                    <th>Estado</th>
                    <th>Fecha</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($pedidos as $pedido)
                    <tr>
                        <td>{{ $pedido->id }}</td>
                        <td>{{$pedido->Cliente->nombre}}</td>
                        <td>
                            @if(strlen( $pedido->descripcion) > 20){{substr($pedido->descripcion, 0 , 20  ) . '...'}}
                            @else {{$pedido->descripcion}}
                            @endif
                        </td>
                        <td class="text-right">
                            <?php $total = 0 ?>
                            @foreach($pedido->Items as $item)
                                <?php  $total = $total + ($item->importe_unitario * $item->cantidad) * (1 - $item->descuento / 100) ?>
                            @endforeach
                            {{"$" .number_format($total,2)}}
                        </td>
                        <td>
                            <?php $descripcion_estado = 'Finalizado'; ?>
                            @foreach($pedido->Items as $item)
                                @if($item->Estado->descripcion != 'Finalizado')
                                    <?php $descripcion_estado = 'Pendiente' ?>
                                @endif
                            @endforeach
                            @if(count($pedido->Items) != 0)
                                {{$descripcion_estado}}
                            @endif
                        </td>
                        <td>{{ date('Y/m/d', strtotime($pedido->fecha)) }}</td>

                        <td>

                            <a href="/pedidos/{{ $pedido['id'] }}/edit" class="btn btn-default btn-sm">Editar</a>

                            <a href="/items/index/{{ $pedido['id'] }}" class="btn btn-default btn-sm">Items</a>
                            @if(count($pedido->Items) == 0)
                                <a href="/pedidos/{{ $pedido['id'] }}" class="btn btn-default btn-sm"
                                   data-method="delete"
                                   data-token="{{ csrf_token() }}"
                                   data-confirm="Está seguro que desea eliminar a pedido con nombre {{ $pedido->nombre }}?">
                                    Eliminar
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay pedidos
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/pedidos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/delete-link.js') }}"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
