@extends('layouts.app')

@section('site-name', 'Editar pedido')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Pedido</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ url('pedidos/' . $pedido->id) }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}


                    <!-- Cliente -->
            <div class="form-group{{ $errors->has('cliente_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Cliente</label>

                <div class="col-md-6">
                    @include('pedidos.select-con-buscador')

                </div>
            </div>

            <!-- Fecha -->
            <div class="form-group{{ $errors->has('fecha') ? ' has-error' : '' }}">
                <label for="fecha" class="control-label col-md-4">Fecha</label>

                <div class="col-md-6">
                    <div class="input-group date" id="datetimepicker1" style="">
                        <input type="text" class="form-control" name="fecha" value="{{ $pedido->fecha }}" autocomplete="off"  />
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6" placeholder="Escriba la descripción">{{ $pedido->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>
        
        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAjTpj9h5ANX5iTQIKxkAhI-zcoPxl8GtY"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/pedidos/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/select-con-buscador/select-con-buscador.js') }}"></script>
    <script>
        $('#intro select').zelect({placeholder: 'Busqueda de cliente...'})
    </script>
@stop
