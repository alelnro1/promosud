@extends('layouts.app')

@section('site-name', 'Viendo a pedido')

@section('content')
    <div class="panel-heading">
        Pedido con nombre <b><i>{{ $pedido->nombre }}</i></b>
    </div>

    <div class="panel-body">

        @if(isset($pedido->archivo) && $pedido->archivo != "")
            <div class="text-center margin-bottom">
                <img src="/{{ $pedido->archivo }}" style="max-height: 150px;" />
            </div>
        @endif

        <table class="table table-striped task-table" style="margin-bottom: 20px;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $pedido->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $pedido->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>{{ $pedido->estado }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('Y/m/d', strtotime($pedido->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio</strong></td>
                <td>{{ $pedido->domicilio }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $pedido->email }}</td>
            </tr>

            <tr>
                <td><strong>Teléfono</strong></td>
                <td>{{ $pedido->telefono }}</td>
            </tr>

            <tr>
                <td><strong>Fecha Creación</strong></td>
                <td>{{ $pedido->created_at }}</td>
            </tr>
        </table>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        <div class="col-xs-6">
            <a href="/pedidos/{{ $pedido->id }}/edit" class="btn btn-default btn-primary" style="float:right; color: white;">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
            </a>
        </div>
    </div>
@stop
