<link rel="stylesheet" href="/css/select-con-buscador.css">

<section id="intro">
    <select class="form-control" id='select-buscador' name="cliente_id"
            value="{{ old('cliente_id') }}">
        @foreach($clientes as $cliente)
            <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
        @endforeach
    </select>
    @if ($errors->has('cliente_id'))
        <span class="help-block">
                            <strong>{{ $errors->first('cliente_id') }}</strong>
                        </span>
    @endif
</section>


