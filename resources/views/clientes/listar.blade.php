@extends('layouts.app')

@section('site-name', 'Listando clientes')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Clientes

        <div style="float:right;">
            <a href="/clientes/create" class="btn btn-block btn-default btn-sm">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('cliente_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('cliente_eliminado') }}
            </div>
        @endif

        @if(Session::has('cliente_creado'))
            <div class="alert alert-success">
                {{ Session::get('cliente_creado') }}
            </div>
        @endif
        
        @if(Session::has('cliente_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('cliente_actualizado') }}
            </div>
        @endif

        @if (count($clientes) > 0)
            <table class="table table-bordered table-hover" id="clientes" style="width:100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Teléfono Fijo</th>
                        <th>Teléfono Celular</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->nombre }}</td>
                        <td>{{ $cliente->email }}</td>
                        <td>{{ $cliente->telefono_fijo }}</td>
                        <td>{{ $cliente->telefono_celular }}</td>

                        <td>
                            <a href="/clientes/{{ $cliente['id'] }}" class="btn btn-default btn-sm">Ver</a>

                            <a href="/clientes/{{ $cliente['id'] }}/edit" class="btn btn-default btn-sm">Editar</a>
                            @if(count($cliente->Pedidos) == 0)
                            <a href="/clientes/{{ $cliente['id'] }}" class="btn btn-default btn-sm"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Está seguro que desea eliminar a cliente con nombre {{ $cliente->nombre }}?">
                                Eliminar
                            </a>
                             @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay clientes
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/clientes/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/delete-link.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
