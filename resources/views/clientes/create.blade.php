@extends('layouts.app')

@section('site-name', 'Nuevo cliente')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="panel-heading">Nuevo</div>

    <div class="panel-body">
        <form action="{{ url('clientes') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre/Razón Social</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}"  placeholder="Escriba el nombre/razon social">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6" placeholder="Escriba la descripción">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Texto -->
            <div class="form-group {{ $errors->has('texto') ? ' has-error' : '' }}">
                <label for="texto" class="control-label col-md-4">Interior</label>

                <div class="col-md-6">
                    <textarea name="texto" rows="3" class="form-control col-md-6" placeholder="Escriba el texto">{{ old('texto') }}</textarea>

                    @if ($errors->has('texto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('texto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Escriba el email">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Telefono Fijo -->
            <div class="form-group{{ $errors->has('telefono_fijo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono Fijo</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono_fijo" value="{{ old('telefono_fijo') }}" placeholder="Escriba el teléfono fijo">

                    @if ($errors->has('telefono_fijo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono_fijo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Telefono Celular -->
            <div class="form-group{{ $errors->has('telefono_celular') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Teléfono Celular</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono_celular" value="{{ old('telefono_celular') }}" placeholder="Escriba el teléfono celular">

                    @if ($errors->has('telefono_celular'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefono_celular') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Persona de Contacto -->
            <div class="form-group{{ $errors->has('persona_contacto') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Persona de Contacto</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="persona_contacto" value="{{ old('persona_contacto') }}" placeholder="Escriba la persona de contacto">

                    @if ($errors->has('persona_contacto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('persona_contacto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Pagina Web -->
            <div class="form-group{{ $errors->has('pagina_web') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Página Web</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="pagina_web" value="{{ old('pagina_web') }}" placeholder="Escriba la pagina web">

                    @if ($errors->has('pagina_web'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pagina_web') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- CUIT -->
            <div class="form-group{{ $errors->has('cuit') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">CUIT</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuit" value="{{ old('cuit') }}" placeholder="Escriba el CUIT">

                    @if ($errors->has('cuit'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuit') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- CUIL -->
            <div class="form-group{{ $errors->has('cuil') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">CUIL</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cuil" value="{{ old('cuil') }}" placeholder="Escriba el CUIL">

                    @if ($errors->has('cuil'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuil') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Foto -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6">
                    <input type="file" class="form-control" name="archivo">

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Domicilio Fiscal -->
            <div class="form-group{{ $errors->has('domicilio_fiscal') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio Fiscal</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio_fiscal" value="{{ old('domicilio_fiscal') }}" id="domicilio_fiscal" autocomplete="off" placeholder="Escriba la dirección fiscal">

                    @if ($errors->has('domicilio_fiscal'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio_fiscal') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <!-- Domicilio Comercial -->
            <div class="form-group{{ $errors->has('domicilio_comercial') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Domicilio Comercial</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="domicilio_comercial" value="{{ old('domicilio_comercial') }}" id="domicilio_comercial" autocomplete="off" placeholder="Escriba la dirección comercial">

                    @if ($errors->has('domicilio_comercial'))
                        <span class="help-block">
                                <strong>{{ $errors->first('domicilio_comercial') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <!-- Condición IVA -->
            <div class="form-group{{ $errors->has('condicion_iva') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Condición frente al IVA</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="condicion_iva" value="{{ old('condicion_iva') }}" id="condicion_iva" autocomplete="off" placeholder="Escriba la condición frente al IVA">

                    @if ($errors->has('condicion_iva'))
                        <span class="help-block">
                                <strong>{{ $errors->first('condicion_iva') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-plus"></i>&nbsp;Crear
                    </button>
                </div>
            </div>
        </form>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAjTpj9h5ANX5iTQIKxkAhI-zcoPxl8GtY"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/clientes/create.js') }}"></script>
@stop
