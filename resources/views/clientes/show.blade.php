@extends('layouts.app')

@section('site-name', 'Viendo a cliente')

@section('content')
    <div class="panel-heading">
        Cliente con nombre <b><i>{{ $cliente->nombre }}</i></b>
    </div>

    <div class="panel-body">

        @if(isset($cliente->archivo) && $cliente->archivo != "")
            <div class="text-center margin-bottom">
                <img src="/{{ $cliente->archivo }}" style="max-height: 150px;" />
            </div>
        @endif

        <table class="table table-striped task-table" style="margin-bottom: 20px;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $cliente->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $cliente->descripcion }}</td>
            </tr>
            <tr>
                <td><strong>Interior</strong></td>
                <td>{{ $cliente->texto }}</td>
            </tr>

            <tr>
                <td><strong>Fecha</strong></td>
                <td>{{ date('d/m/Y', strtotime($cliente->fecha)) }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio Fiscal</strong></td>
                <td>{{ $cliente->domicilio_fiscal }}</td>
            </tr>

            <tr>
                <td><strong>Domicilio Comercial</strong></td>
                <td>{{ $cliente->domicilio_comercial }}</td>
            </tr>

            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $cliente->email }}</td>
            </tr>

            <tr>
                <td><strong>Teléfono Fijo</strong></td>
                <td>{{ $cliente->telefono_fijo }}</td>
            </tr>

            <tr>
                <td><strong>Teléfono Celular</strong></td>
                <td>{{ $cliente->telefono_celular }}</td>
            </tr>

            <tr>
                <td><strong>Fecha Creación</strong></td>
                <td>{{ $cliente->created_at }}</td>
            </tr>
        </table>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        <div class="col-xs-6">
            <a href="/clientes/{{ $cliente->id }}/edit" class="btn btn-default btn-primary" style="float:right; color: white;">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
            </a>
        </div>
    </div>
@stop
