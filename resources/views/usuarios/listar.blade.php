@extends('layouts.app')

@section('site-name', 'Listando usuarios')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Usuarios

        <div style="float:right;">
            <a href="/usuarios/create" class="btn btn-block btn-default btn-sm">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('usuario_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_eliminado') }}
            </div>
        @endif

        @if(Session::has('usuario_creado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_creado') }}
            </div>
        @endif
        
        @if(Session::has('usuario_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_actualizado') }}
            </div>
        @endif

        @if (count($usuarios) > 0)
            <table class="table table-bordered table-hover" id="usuarios" style="width:100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Fecha</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td>{{ $usuario->name }}</td>
                        <td>{{ $usuario->email }}</td>
                        <td>{{ date('Y/m/d', strtotime($usuario->created_at)) }}</td>

                        <td>
                            <a href="/usuarios/{{ $usuario['id'] }}/edit" class="btn btn-default btn-sm">Editar</a>
                            
                            <a href="/usuarios/{{ $usuario['id'] }}" class="btn btn-default btn-sm"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Está seguro que desea eliminar a usuario con nombre {{ $usuario->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay usuarios
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/usuarios/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/delete-link.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
