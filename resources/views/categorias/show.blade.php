@extends('layouts.app')

@section('site-name', 'Viendo a categoria')

@section('content')
    <div class="panel-heading">
        Categoria con nombre <b><i>{{ $categoria->nombre }}</i></b>
    </div>

    <div class="panel-body">

        @if(isset($categoria->archivo) && $categoria->archivo != "")
            <div class="text-center margin-bottom">
                <img src="/{{ $categoria->archivo }}" style="max-height: 150px;" />
            </div>
        @endif

        <table class="table table-striped task-table" style="margin-bottom: 20px;">

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $categoria->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>@if($categoria->estado == "on")Activo @else Inactivo @endif</td>
            </tr>

        </table>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        <div class="col-xs-6">
            <a href="/categorias/{{ $categoria->id }}/edit" class="btn btn-default btn-primary" style="float:right; color: white;">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
            </a>
        </div>
    </div>
@stop
