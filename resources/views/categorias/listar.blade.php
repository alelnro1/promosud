@extends('layouts.app')

@section('site-name', 'Listando categorias')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Categorias

        <div style="float:right;">
            <a href="/categorias/create" class="btn btn-block btn-default btn-sm">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('categoria_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('categoria_eliminado') }}
            </div>
        @endif

        @if(Session::has('categoria_creado'))
            <div class="alert alert-success">
                {{ Session::get('categoria_creado') }}
            </div>
        @endif
        
        @if(Session::has('categoria_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('categoria_actualizado') }}
            </div>
        @endif

        @if (count($categorias) > 0)
            <table class="table table-bordered table-hover" id="categorias" style="width:100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($categorias as $categoria)
                    <tr>
                        <td style="">{{ $categoria->descripcion }}</td>
                        <td>@if($categoria->estado == "on")Activo @else Inactivo @endif</td>

                        <td>
                            <a href="/categorias/{{ $categoria['id'] }}" class="btn btn-default btn-sm">Ver</a>
                            
                            <a href="/categorias/{{ $categoria['id'] }}/edit" class="btn btn-default btn-sm">Editar</a>
                            
                            <a href="/categorias/{{ $categoria['id'] }}" class="btn btn-default btn-sm"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Está seguro que desea eliminar a categoria con nombre {{ $categoria->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay categorias
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/categorias/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/delete-link.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
