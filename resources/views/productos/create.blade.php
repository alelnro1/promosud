@extends('layouts.app')

@section('site-name', 'Nuevo producto')

@section('styles')
    <link href="{{ asset('css/fileinput.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="panel-heading">Nuevo</div>

    <div class="panel-body">
        <form action="{{ url('productos') }}" method="POST" class="form-horizontal" id="formulario" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}"  placeholder="Escriba el nombre">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6" placeholder="Escriba la descripción">{{ old('descripcion') }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- categoria  -->
            <div class="form-group{{ $errors->has('categoria_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Categoría</label>

                <div class="col-md-6">
                    <select class="form-control" name="categoria_id" >
                        <option value="">Seleccione una categoría</option>
                        @foreach($categorias as $categoria)
                        <option value="{{$categoria->id}}"> {{$categoria->descripcion }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('categoria_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('categoria_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <!-- Estado -->
            <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="estado">Estado</label>

                <div class="col-md-6">
                    <input type="checkbox" checked name="estado"> Estado
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <!-- costo  -->
            <div class="form-group{{ $errors->has('costo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Costo</label>

                <div class="col-md-6">
                    <input step="0.01" type="number" class="form-control" name="costo" value="{{ old('costo') }}"  placeholder="Escriba el Costo">

                    @if ($errors->has('costo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('costo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Archivo</label>

                <div class="col-md-6" >
                    <div class="form-group">
                        <input  type="file" multiple id="file12"  name="archivo[]">
                    </div>

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Rangos -->
            <div  class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Rangos</label>

                <div class="col-md-6">
                    <a id="agregarCampo" href="">Agregar Campos</a>
                    <div class="row ">
                        <div class="col-md-4 rango" >
                            <input type="number" class="form-control" name="desde[]"    placeholder="Desde">
                        </div>
                        <div class="col-md-4 rango">
                            <input type="number" class="form-control" name="hasta[]"    placeholder="Hasta">
                        </div>
                        <div class="col-md-4 rango">
                            <input step="0.01" type="number" class="form-control" name="importe[]"  placeholder="Importe">
                        </div>
                    </div>
                    <br>
                    <div id="div_a_repetir"></div>


                    @if ($errors->has('rango'))
                        <span class="help-block">
                            <strong>{{ $errors->first('rango') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-plus"></i>&nbsp;Crear
                    </button>
                </div>
            </div>



        </form>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/productos/create.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/productos/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/productos/agregar-campos-dinamicos.js') }}"></script>


@stop
