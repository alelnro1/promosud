@extends('layouts.app')

@section('site-name', 'Listando productos')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css">
@stop

@section('content')
    <div class="panel-heading">
        Productos

        <div style="float:right;">
            <a href="/productos/create" class="btn btn-block btn-default btn-sm">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('producto_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('producto_eliminado') }}
            </div>
        @endif

        @if(Session::has('producto_creado'))
            <div class="alert alert-success">
                {{ Session::get('producto_creado') }}
            </div>
        @endif
        
        @if(Session::has('producto_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('producto_actualizado') }}
            </div>
        @endif

        @if (count($productos) > 0)
            <table class="table table-bordered table-hover" id="productos" style="width:100%">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Costo Unitario</th>
                        <th>Estado</th>
                        <th>Categoria</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto->nombre }}</td>
                        <td>{{'$ ' . number_format($producto->costo,2) }}</td>
                        <td>@if($producto->estado == 'on') Activo @else Inactivo @endif</td>
                        <td>{{ $producto->Categoria->descripcion }}</td>

                        <td>
                            <a href="/productos/{{ $producto['id'] }}" class="btn btn-default btn-sm">Ver</a>
                            
                            <a href="/productos/{{ $producto['id'] }}/edit" class="btn btn-default btn-sm">Editar</a>
                            
                            <a href="/productos/{{ $producto['id'] }}" class="btn btn-default btn-sm"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Está seguro que desea eliminar a producto con nombre {{ $producto->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay productos
        @endif
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/productos/listar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/delete-link.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
@stop
