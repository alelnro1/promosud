@extends('layouts.app')

@section('site-name', 'Editar producto')

@section('styles')
    <link href="{{ asset('css/fileinput.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="panel-heading">Producto</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" id="formulario" action="{{ url('productos/' . $producto->id) }}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}


                    <!-- Nombre -->
            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $producto->nombre }}"  placeholder="Escriba el nombre">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Descripcion -->
            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label col-md-4">Descripción</label>

                <div class="col-md-6">
                    <textarea name="descripcion" rows="3" class="form-control col-md-6" placeholder="Escriba la descripción">{{ $producto->descripcion }}</textarea>

                    @if ($errors->has('descripcion'))
                        <span class="help-block">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- categoria  -->
            <div class="form-group{{ $errors->has('categoria_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Categoría</label>

                <div class="col-md-6">
                    <select class="form-control" name="categoria_id" >
                        <option value="">Seleccione una categoría</option>
                        @foreach($categorias as $categoria)
                            <option value="{{$categoria->id}}" @if($producto->categoria_id == $categoria->id) selected @endif> {{$categoria->descripcion }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('categoria_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('categoria_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>



            <!-- Estado -->
            <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                <label class="control-label col-md-4" for="estado">Estado</label>

                <div class="col-md-6">
                    <input type="checkbox" @if($producto->estado == 'on') checked @endif name="estado"> Estado
                </div>
                <div style="clear:both;"></div><br>
            </div>

            <!-- costo  -->
            <div class="form-group{{ $errors->has('costo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Costo</label>

                <div class="col-md-6">
                    <input step="0.01" type="number" class="form-control" name="costo" value="{{ $producto->costo }}"  placeholder="Escriba el Costo">

                    @if ($errors->has('costo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('costo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group text-center imagenes">
            <div class="col-md-10">
            @if(isset($producto->ImagenesProductos) && $producto->ImagenesProductos != "")
                @foreach($producto->ImagenesProductos as $imagen)
                    <div class="margin-bottom" id="delete{{$imagen->id}}">
                        <a href="#" class="eliminar-imagen" data-imagen-producto-id="{{$imagen->id}}"><img src="/{{ $imagen->archivo }}" style="max-height: 40px;"> Eliminar</a>
                    </div>
                 @endforeach
            @endif
            </div>
            </div>

            <!-- Archivo -->
            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label col-md-4">Nuevos Archivo</label>

                <div class="col-md-6" >
                    <div class="form-group">
                        <input  type="file" multiple id="file12"  name="archivo[]">
                    </div>

                    @if ($errors->has('archivo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('archivo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Rangos -->
            <div  class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Rangos</label>

                <div class="col-md-6">
                    <a id="agregarCampo" href="">Agregar Campos</a>

                    @foreach($producto->Rangos as $index=>$rango)
                        <br>
                        <div class="row rango">
                            <div class="col-md-4 rango" >
                                <input type="number" class="form-control" name="desde[]"  value="{{$rango->desde}}" placeholder="Desde">
                            </div>
                            <div class="col-md-4 rango">
                                <input type="number" class="form-control" name="hasta[]"  value="{{$rango->hasta}}"  placeholder="Hasta">
                            </div>
                            <div class="col-md-4 rango">
                                <input step="0.01" type="number" class="form-control" name="importe[]"  value="{{$rango->importe}}" placeholder="Importe">
                            </div>
                            @if($index > 0)
                                <a href="#" class="eliminar-precargado">&times;</a>
                            @endif
                        </div>

                    @endforeach

                    <div id="div_a_repetir"></div>


                    @if ($errors->has('rango'))
                        <span class="help-block">
                            <strong>{{ $errors->first('rango') }}</strong>
                        </span>
                    @endif
                </div>

            </div>


            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;Actualizar
                    </button>
                </div>
            </div>
        </form>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/productos/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/productos/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/productos/agregar-campos-dinamicos.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.eliminar-imagen').on('click', function(){
                var imagen_producto_id = $(this).data('imagen-producto-id');

                $.ajax({
                    url: '/eliminar-imagen-producto/' + imagen_producto_id,
                    dataType: 'json',
                    success: function(data){
                        if (data.valid == true)
                        $('#delete'+ imagen_producto_id ).fadeOut(1000);
                    }
                });
            });
        });
    </script>

@stop
