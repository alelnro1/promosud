@extends('layouts.app')

@section('site-name', 'Viendo a producto')

@section('content')
    <div class="panel-heading">
        Producto con nombre <b><i>{{ $producto->nombre }}</i></b>
    </div>

    <div class="panel-body">
        @if(isset($producto->ImagenesProductos) && $producto->ImagenesProductos != "")
            @foreach($producto->ImagenesProductos as $imagen)
            <div class="text-center margin-bottom col-md-3">
                <img src="/{{ $imagen->archivo }}" style="max-height: 150px;" />
            </div>
            @endforeach
        @endif

        <table class="table table-striped task-table" style="margin-bottom: 20px;">
            <tr>
                <td><strong>Nombre</strong></td>
                <td>{{ $producto->nombre }}</td>
            </tr>

            <tr>
                <td><strong>Descripción</strong></td>
                <td>{{ $producto->descripcion }}</td>
            </tr>

            <tr>
                <td><strong>Estado</strong></td>
                <td>@if($producto->estado == 'on') Activo @else Inactivo @endif</td>
            </tr>

            <tr>
                <td><strong>Costo Unitario</strong></td>
                <td>{{'$ ' . $producto->costo }}</td>
            </tr>

            <tr>
                <td><strong>Categoría</strong></td>
                <td>{{ $producto->Categoria->descripcion }}</td>
            </tr>
            <tr>
                <td><strong>Rangos</strong></td>
                <td>
                    <table class="table table-striped task-table" >
                        <thead>
                        <tr>
                            <th>Desde</th>
                            <th>Hasta</th>
                            <th>Importe</th>
                        </tr>
                        </thead>
                    @foreach($producto->Rangos as $rango)
                        <tr>
                            <td>{{$rango->desde}} </td>
                            <td>{{$rango->hasta}} </td>
                            <td>{{'$ ' . $rango->importe}} </td>
                        </tr>
                    @endforeach
                    </table>
                </td>
            </tr>

            <tr>
                <td><strong>Fecha De Carga</strong></td>
                <td>
                    {{
                        date('d/m/Y', strtotime($producto->created_at))
                    }}
                </td>
            </tr>

        </table>

        <div class="pull-xs-left col-xs-6">
            <a href="#" onclick="window.history.go(-1); return false;" class="btn btn-default">
                <i class="fa fa-fw fa-arrow-left"></i>&nbsp;Volver
            </a>
        </div>

        <div class="col-xs-6">
            <a href="/productos/{{ $producto->id }}/edit" class="btn btn-default btn-primary" style="float:right; color: white;">
                <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Editar
            </a>
        </div>
    </div>
@stop
